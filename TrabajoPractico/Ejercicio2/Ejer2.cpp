#include <SFML/Graphics.hpp>
#include<iostream>
using namespace std;

int main()
{
	sf::RenderWindow window(sf::VideoMode(500, 500), "SFML works!");
	//sf::CircleShape shape(100.f);
	//shape.setFillColor(sf::Color::Green);
	sf::Texture texture;
	

	if (!texture.loadFromFile("Lucario.png"))
	{
		cout<< "Error loading file" <<endl;
	}

	sf::Sprite sprite(texture);
	
	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			switch (event.type)
			{
				case sf::Event::Closed:

					window.close();

					break;

				case sf::Event::KeyPressed:
					cout << "Key has been pressed" << endl;

					break;

			}

		}

		window.clear();
		window.draw(sprite);
		//window.draw(shape);
		window.display();
	}

	return 0;
}